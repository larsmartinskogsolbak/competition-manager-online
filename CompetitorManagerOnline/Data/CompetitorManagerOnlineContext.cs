﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using CompetitorManagerOnline.Models;

namespace CompetitorManagerOnline.Data
{
    public class CompetitorManagerOnlineContext : DbContext
    {
        public CompetitorManagerOnlineContext (DbContextOptions<CompetitorManagerOnlineContext> options)
            : base(options)
        {
        }

        public DbSet<CompetitorManagerOnline.Models.Competitor> Competitor { get; set; }
    }
}
